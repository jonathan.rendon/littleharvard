<?php
include "php/lang.php";
$idioma = empty($_POST['idioma']) ? 'EN' : $_POST['idioma']; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Little Hervard Learning Center - DayCare"/>
    <meta name="keywords" content="DayCare,Niños,VPK,Kissimmee">
    <meta name="author" content="xprmarketinggroup">
    <!-- Page title -->
    <title>Little Harvard Learning Center</title>
    <!--[if lt IE 9]>
    <script src="js/respond.js"></script>
    <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Icon fonts -->
    <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="fonts/flaticons/flaticon.css" rel="stylesheet" type="text/css">
    <link href="fonts/glyphicons/bootstrap-glyphicons.css" rel="stylesheet" type="text/css">
    <!-- Google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:700,900' rel='stylesheet' type='text/css'>
    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Color Style CSS -->
    <link href="styles/funtime.css" rel="stylesheet">
    <!-- Owl Slider & Prettyphoto -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/prettyPhoto.css">
    <!-- LayerSlider stylesheet -->
    <link rel="stylesheet" href="layerslider/css/layerslider.css">
    <!-- Favicons-->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">



</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
<!-- Page width 'Boxed' of 'Full' -->
<div class="full">
    <!-- Preloader -->
    <div id="preloader">
        <div class="preloader">
            <img src="img/logogrande.png" alt="" width="350px;">
        </div>
    </div>
    <!-- Navbar -->
    <nav class="navbar navbar-custom navbar-fixed-top">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-brand-centered">
                <i class="fa fa-bars"></i>
            </button>
            <div class="navbar-brand-centered page-scroll">
                <a href="#page-top"><img src="img/logo.png" alt=""></a>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-brand-centered">
            <div class="container">
                <ul class="nav navbar-nav page-scroll navbar-left">
                    <li><a href="#page-top"><?php echo $lang[$idioma]['HOME']; ?></a></li>
                    <li><a href="#mision"><?php echo $lang[$idioma]['MISION']; ?></a></li>
                    <li><a href="#about"><?php echo $lang[$idioma]['ABOUT']; ?></a></li>
                    <li><a href="#team"><?php echo $lang[$idioma]['TEAM']; ?></a></li>
                </ul>
                <ul class="nav navbar-nav page-scroll navbar-right">
                    <li><a href="#activities"><?php echo $lang[$idioma]['PROGRAMS']; ?></a></li>
                    <li><a href="#prices"><?php echo $lang[$idioma]['SAFETY']; ?></a></li>
                    <!-- Dropdown -->
                    <li class="dropdown active">
                        <a href="#" class="dropdown-toggle"
                           data-toggle="dropdown"><?php echo $lang[$idioma]['PARENT']; ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <!--  <li><a href="#"><?php /*echo $lang[$idioma]['TUITION']; */?></a></li>
                            <li><a href="#calendar"><?php /*echo $lang[$idioma]['CALENDAR']; */?></a></li>
                            <li><a href="#formparent"><?php echo $lang[$idioma]['FORM']; ?></a></li>
                            <li><a href="#enroll"><?php echo $lang[$idioma]['ENROLL']; ?></a></li>-->
                            <li><a href="#gallery"><?php echo $lang[$idioma]['GALLERY']; ?></a></li>
                            <li><a href="#contact"><?php echo $lang[$idioma]['CONTACT']; ?></a></li>
                        </ul>
                    </li>


                 <!--   <li>
                        <form action="index.php" method="post">
                            <select name="idioma" onchange="this.form.submit()" id="idioma" size="1"
                                <option value="ES" <?php /*if ($idioma == 'ES') {
                                    echo "selected";
                                } */?>><?php /*echo $lang[$idioma]['SPANISH']; */?></option>
                                <option value="EN" <?php /*if ($idioma == 'EN') {
                                    echo "selected";
                                } */?>><?php /*echo $lang[$idioma]['ENGLISH']; */?></option>
                            </select>
                        </form>
                    </li>-->
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <!-- /navbar ends -->

    <!-- Slider -->
    <div id="layerslider">
        <!-- Slide 1 -->
        <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
            <!-- Background image -->
            <img src="img/slide6.jpg" class="ls-bg" alt="Slide background"/>
            <!-- Parallax Image -->
            <img src="img/sun.png" class="ls-l img-responsive hidden-xs hidden-sm parallax1" alt=""
                 data-ls="delayin:1000;easingin:fadeIn;parallaxlevel:7;">
            <!-- Text -->
            <div class="ls-l header-text container"
                 data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;parallaxlevel:2;">
                <h1>Welcome</h1>
                <p class="subtitle hidden-xs"> Little Harvard Learning Center is a Super Fun Daycare
                    Elements </p>
                <!-- Button -->
                <div class="page-scroll hidden-xs">
                    <a class="btn" href="#contact"><?php echo $lang[$idioma]['CONTACT']; ?></a>
                </div>
            </div>
            <!-- Parallax Image -->
            <img src="img/flower.png" class="ls-l img-responsive hidden-xs hidden-sm parallax2" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:6;">
        </div>

        <!-- Slide 2 -->
        <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
            <!-- Background image -->
            <img src="img/slide2.jpg" class="ls-bg" alt="Slide background"/>
            <!-- Parallax Image -->
            <img src="img/bee.png" class="ls-l img-responsive hidden-xs hidden-sm parallax1" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:7;">
            <!-- Text -->
            <div class="ls-l header-text"
                 data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;parallaxlevel:2;">
                <h1>Quality daycare</h1>
                <p class="subtitle hidden-xs"> Little Harvard Learning Center is a Super Fun Daycare
                    Elements </p>
                <!-- Button -->
                <div class="page-scroll hidden-xs">
                    <a class="btn" href="#contact"><?php echo $lang[$idioma]['CONTACT']; ?></a>
                </div>
            </div>
            <!-- Parallax Image -->
            <img src="img/star.png" class="ls-l img-responsive hidden-xs hidden-sm parallax2" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:6;">
        </div>

        <!-- Slide 3 -->
        <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
            <!-- Background image -->
            <img src="img/slide3.jpg" class="ls-bg" alt="Slide background"/>
            <!-- Parallax Image -->
            <img src="img/sun.png" class="ls-l img-responsive hidden-xs hidden-sm parallax1 dibujo_izquierda_arriba" alt=""
                 data-ls="delayin:1000;easingin:fadeIn;parallaxlevel:7;">
            <!-- Text -->
            <div class="ls-l header-text cartel_izquierda"
                 data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;parallaxlevel:2;">
                <h1>Welcome</h1>
                <p class="subtitle hidden-xs"> Little Harvard Learning Center is a Super Fun Daycare
                    Elements </p>
                <!-- Button -->
                <div class="page-scroll hidden-xs">
                    <a class="btn" href="#contact"><?php echo $lang[$idioma]['CONTACT']; ?></a>
                </div>
            </div>
            <!-- Parallax Image -->
            <img src="img/flower.png" class="ls-l img-responsive hidden-xs hidden-sm parallax2 dibujo_izquierda_abajo" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:6;">
        </div>

        <!-- Slide 4 -->
        <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
            <!-- Background image -->
            <img src="img/slide4.jpg" class="ls-bg" alt="Slide background"/>
            <!-- Parallax Image -->
            <img src="img/bee.png" class="ls-l img-responsive hidden-xs hidden-sm parallax1 dibujo_izquierda_arriba" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:7;">
            <!-- Text -->
            <div class="ls-l header-text cartel_izquierda"
                 data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;parallaxlevel:2;">
                <h1>Quality daycare</h1>
                <p class="subtitle hidden-xs"> Little Harvard Learning Center is a Super Fun Daycare
                    Elements </p>
                <!-- Button -->
                <div class="page-scroll hidden-xs">
                    <a class="btn" href="#contact"><?php echo $lang[$idioma]['CONTACT']; ?></a>
                </div>
            </div>
            <!-- Parallax Image -->
            <img src="img/star.png" class="ls-l img-responsive hidden-xs hidden-sm parallax2 dibujo_izquierda_abajo" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:6;">
        </div>

        <!-- Slide 5 -->
        <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
            <!-- Background image -->
            <img src="img/slide5.jpg" class="ls-bg" alt="Slide background"/>
            <!-- Video
            <div class="ls-l video" data-ls="delayin:1500;easingin:fadeIn;">
                <iframe width="760" height="475" src="https://www.youtube.com/embed/e9p0xmsU3h0"
                        allowfullscreen></iframe>
            </div>-->
            <!-- Parallax Image -->
            <img src="img/bee.png" class="ls-l img-responsive hidden-xs hidden-sm parallax1" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:7;">
            <!-- Text -->
            <div class="ls-l header-text"
                 data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;parallaxlevel:2;">
                <h1>Video Layer</h1>
                <p class="subtitle hidden-xs"> Little Harvard Learning Center is a Super Fun Daycare
                    Elements </p>
                <!-- Button -->
                <div class="page-scroll hidden-xs">
                    <a class="btn" href="#contact"><?php echo $lang[$idioma]['CONTACT']; ?></a>
                </div>
            </div>
            <!-- Parallax Image -->
            <img src="img/star.png" class="ls-l img-responsive hidden-xs hidden-sm parallax2" alt=""
                 data-ls="delayin:1500;easingin:fadeIn;parallaxlevel:6;">
        </div>
    </div>
    <!-- /Layerslider ends-->

    <!-- Clouds SVG Divider -->
    <div class="hidden-xs container-fluid cloud-divider">
        <svg id="deco-clouds1" class="head" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100"
             viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M-5 100 Q 0 20 5 100 Z
               M0 100 Q 5 0 10 100
               M5 100 Q 10 30 15 100
               M10 100 Q 15 10 20 100
               M15 100 Q 20 30 25 100
               M20 100 Q 25 -10 30 100
               M25 100 Q 30 10 35 100
               M30 100 Q 35 30 40 100
               M35 100 Q 40 10 45 100
               M40 100 Q 45 50 50 100
               M45 100 Q 50 20 55 100
               M50 100 Q 55 40 60 100
               M55 100 Q 60 60 65 100
               M60 100 Q 65 50 70 100
               M65 100 Q 70 20 75 100
               M70 100 Q 75 45 80 100
               M75 100 Q 80 30 85 100
               M80 100 Q 85 20 90 100
               M85 100 Q 90 50 95 100
               M90 100 Q 95 25 100 100
               M95 100 Q 100 15 105 100 Z">
            </path>
        </svg>
    </div>
    <!-- / section ends-->

    <!-- Section Mision -->
    <section id="mision" style="background: #e6f2f7;">
        <div class="container">
            <div class="paper_block">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- Section heading -->
                    <div class="section-heading">
                        <h2><?php echo $lang[$idioma]['MISION']; ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-5">
                        <!-- Carousel -->
                        <div id="owl-mision" class="owl-carousel">
                            <div class="item">
                                <img class="img-responsive" src="img/about4.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="img/about2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="img/about3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- text -->
                    <div class="col-lg-7 col-sm-12">
                        <p><?php echo $lang[$idioma]['TEXT1_MISSION']; ?></p>
                        <p><?php echo $lang[$idioma]['SUBTITLE1_MISSION']; ?></p>

                        <p><i class="fa fa-pencil"></i><?php echo $lang[$idioma]['LIST1_MISSION']; ?></p>
                        <p><i class="fa fa-pencil"></i><?php echo $lang[$idioma]['LIST2_MISSION']; ?></p>
                        <p><i class="fa fa-pencil"></i><?php echo $lang[$idioma]['LIST3_MISSION']; ?></p>
                        <p><?php echo $lang[$idioma]['SUBTITLE2_MISSION']; ?></p>

                        <p><i class="fa fa-pencil"></i><?php echo $lang[$idioma]['LIST1_MISSION2']; ?></p>
                        <p><i class="fa fa-pencil"></i><?php echo $lang[$idioma]['LIST2_MISSION2']; ?></p>
                        <p><i class="fa fa-pencil"></i><?php echo $lang[$idioma]['LIST3_MISSION2']; ?></p>
                    </div>
                    <!-- /col-lg-8 -->
                </div>
            </div>
        </div>
        <!--/container-->
    </section>
    <!--/ Section ends -->


    <!-- Section About -->
    <section id="about" style="background-image: url('img/bg1.png');">
        <div class="container">
            <div class="color_block">
            <div class="col-lg-8 col-lg-offset-2">
                <!-- Section heading -->
                <div class="section-heading">
                    <h2><?php echo $lang[$idioma]['ABOUT']; ?></h2>
                </div>
            </div>
            <div class="row">
                <!-- text -->
                <div class="col-lg-7 col-sm-12">
                    <p><?php echo $lang[$idioma]['TEXT1_ABOUT']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT2_ABOUT']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT3_ABOUT']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT4_ABOUT']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT5_ABOUT']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT6_ABOUT']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT7_ABOUT']; ?></p>
                </div>
                <!-- /col-lg-8 -->
                <div class="col-sm-12 col-lg-5">
                    <!-- Carousel -->
                    <div id="owl-about" class="owl-carousel">
                        <div class="item">
                            <img class="img-responsive" src="img/about5.jpg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/about6.jpg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/about7.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!--/container-->
    </section>
    <!--/ Section ends -->

    <!-- Parallax object -->
    <div class="parallax-object1 hidden-sm hidden-xs" data-0="opacity:1;"
         data-100="transform:translatey(40%);"
         data-center-center="transform:translatey(-180%);">
        <!-- Image -->
        <img src="img/parallaxobject1.png" alt="">
    </div>

    <!-- Section Team -->
    <section id="team" class="color-section">
        <!-- svg triangle shape -->
        <svg class="triangleShadow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100"
             viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="trianglePath1" d="M0 0 L50 100 L100 0 Z"/>
        </svg>
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <!-- Section heading -->
                <div class="section-heading">
                    <h2><?php echo $lang[$idioma]['TEAM']; ?></h2>
                </div>
            </div>
            <!-- Intro text -->
            <div class="row team">
                <div class="col-lg-12 col-md-12 res-margin">
                    <img src="img/teammain.jpg" class="center-block img-responsive img-curved" alt=""/>
                </div>
                <div class="col-lg-7 col-md-7">
                    <p> <?php echo $lang[$idioma]['TEXT1_TEAM']; ?></p>
                </div>
            </div>
            <!-- Team Carousel-->
            <div id="owl-team" class="owl-carousel">
                <div class="col-lg-12">
                    <!-- member 1-->
                    <div class="team-item">
                        <img src="img/team1.jpg" alt=""/>
                        <div class="team-caption" style="background-color:#23527c;">
                            <h5 class="text-light">Laura Kappos</h5>
                            <p style="font-size: 10px;"><?php echo $lang[$idioma]['ADMIN_TEAM']; ?></p>
                        </div>
                    </div>
                    <!-- /team-item-->
                </div>
                <!-- col-lg-12-->
                <div class="col-lg-12">
                    <!-- member 2-->
                    <div class="team-item">
                        <img src="img/team2.jpg" alt=""/>
                        <div class="team-caption" style="background-color:#23527c;">
                            <h5 class="text-light">Belkis Ruano</h5>
                            <p style="font-size: 10px;"><?php echo $lang[$idioma]['DIRECTOR_TEAM']; ?></p>
                        </div>
                    </div>
                    <!-- /team-item-->
                </div>
                <!-- col-lg-12-->
            </div>
            <!--/owl-team -->
        </div>
        <!--/container -->
    </section>
    <!-- Section ends -->

    <!-- Section activities -->
    <section id="activities">
        <div class="container">
            <!-- Section Heading -->
            <div class="section-heading">
                <h2><?php echo $lang[$idioma]['PROGRAMS']; ?></h2>
            </div>
            <div class="row">
            <!--Navigation -->
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#tab1" data-toggle="tab" style="font-size: 14px;"><?php echo $lang[$idioma]['SUBTITLE1_PROGRAM']; ?></a></li>
                <li><a href="#tab2" data-toggle="tab" style="font-size: 14px;"><?php echo $lang[$idioma]['SUBTITLE2_PROGRAM']; ?></a></li>
                <li><a href="#tab3" data-toggle="tab" style="font-size: 14px;"><?php echo $lang[$idioma]['SUBTITLE3_PROGRAM']; ?></a></li>
                <li><a href="#tab4" data-toggle="tab" style="font-size: 14px;"><?php echo $lang[$idioma]['SUBTITLE4_PROGRAM']; ?></a></li>
                <li><a href="#tab5" data-toggle="tab" style="font-size: 14px;"><?php echo $lang[$idioma]['SUBTITLE5_PROGRAM']; ?></a></li>
                <li><a href="#tab6" data-toggle="tab" style="font-size: 14px;"><?php echo $lang[$idioma]['SUBTITLE6_PROGRAM']; ?></a></li>
            </ul>
            <div class="tabbable">
                <div class="tab-content col-md-12 col-centered">
                    <!--Tab Content 1 -->
                    <div class="tab-pane active in fade" id="tab1">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 pull-left">
                                <!-- Activity image-->
                                <img src="img/activity1.jpg" alt="" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-7 col-lg-7 pull-left">
                                <!-- Activity text-->
                                <h3><?php echo $lang[$idioma]['SUBTITLE1_PROGRAM']; ?></h3>
                                <p><?php echo $lang[$idioma]['TEXT1_PROGRAM']; ?></p>

                            </div>
                            <!-- /.col-md-7 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab1 -->
                    <!--Tab Content 2 -->
                    <div class="tab-pane fade" id="tab2">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 pull-left">
                                <!-- Activity image-->
                                <img src="img/activity2.jpg" alt="" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-7 col-lg-7 pull-left">
                                <!-- Activity text-->
                                <h3><?php echo $lang[$idioma]['SUBTITLE2_PROGRAM']; ?></h3>
                                <p><?php echo $lang[$idioma]['TEXT2_PROGRAM']; ?></p>
                            </div>
                            <!-- /.col-md-7 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab2 -->
                    <!--Tab Content 3 -->
                    <div class="tab-pane fade" id="tab3">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 pull-left">
                                <!-- Activity image-->
                                <img src="img/activity3.jpg" alt="" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-7 col-lg-7 pull-left">
                                <!-- Activity text-->
                                <h3><?php echo $lang[$idioma]['SUBTITLE3_PROGRAM']; ?></h3>
                                <p><?php echo $lang[$idioma]['TEXT3_PROGRAM']; ?></p>
                            </div>
                            <!-- /.col-md-7 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab3 -->
                    <!--Tab Content 4 -->
                    <div class="tab-pane fade" id="tab4">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 pull-left">
                                <!-- Activity image-->
                                <img src="img/activity4.jpg" alt="" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-7 col-lg-7 pull-left">
                                <!-- Activity text-->
                                <h3><?php echo $lang[$idioma]['SUBTITLE4_PROGRAM']; ?></h3>
                                <p><?php echo $lang[$idioma]['TEXT4_PROGRAM']; ?></p>
                            </div>
                            <!-- /.col-md-7 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab4 -->
                    <!--Tab Content 5 -->
                    <div class="tab-pane fade" id="tab5">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 pull-left">
                                <!-- Activity image-->
                                <img src="img/activity5.jpg" alt="" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-7 col-lg-7 pull-left">
                                <!-- Activity text-->
                                <h3><?php echo $lang[$idioma]['SUBTITLE5_PROGRAM']; ?></h3>
                                <p><?php echo $lang[$idioma]['TEXT5_PROGRAM']; ?></p>
                            </div>
                            <!-- /.col-md-7 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab5 -->
                    <!--Tab Content 6 -->
                    <div class="tab-pane fade" id="tab6">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 pull-left">
                                <!-- Activity image-->
                                <img src="img/activity6.jpg" alt="" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-7 col-lg-7 pull-left">
                                <!-- Activity text-->
                                <h3><?php echo $lang[$idioma]['SUBTITLE6_PROGRAM']; ?></h3>
                                <p><?php echo $lang[$idioma]['TEXT6_PROGRAM']; ?></p>
                            </div>
                            <!-- /.col-md-7 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab6 -->
                </div>
                <!--tab-content-->
            </div>
            <!--tababble-->
            </div>
            <div class="row">
                <!-- main text -->
                <div class="col-md-12 text-center">
                    <p><?php echo $lang[$idioma]['TEXT7_PROGRAM']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT8_PROGRAM']; ?></p>
                    <p><?php echo $lang[$idioma]['TEXT9_PROGRAM']; ?></p>
                </div>
            </div>
        </div>
        <!-- /container -->
    </section>
    <!-- /Section ends -->

    <!-- Section ends -->
    <!-- Parallax object -->
    <div class="parallax-object3 hidden-sm hidden-xs" data-0="opacity:1;"
         data-100="transform:translatex(0%);"
         data-center-center="transform:translatex(380%);">
        <!-- Image -->
        <img src="img/parallaxobject3.png" alt="">
    </div>

    <!-- Section Prices -->
    <section id="prices" class="color-section">
        <!-- svg triangle shape -->
        <svg id="triangleShadow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100"
             viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="trianglePath1" d="M0 0 L50 100 L100 0 Z"/>
        </svg>
        <div class="container">
            <div class="paper_block">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- Section heading -->
                    <div class="section-heading">
                        <h2><?php echo $lang[$idioma]['SAFETY']; ?></h2>
                    </div>
                </div>
                <div class="row">
                    <!-- text -->
                    <div class="col-lg-12 col-sm-12">
                        <p><i class="fa fa-support"></i><?php echo $lang[$idioma]['LIST1_SAFE']; ?></p>
                        <p><i class="fa fa-support"></i><?php echo $lang[$idioma]['LIST2_SAFE']; ?></p>
                        <p><i class="fa fa-support"></i><?php echo $lang[$idioma]['LIST3_SAFE']; ?></p>
                        <p><i class="fa fa-support"></i><?php echo $lang[$idioma]['LIST4_SAFE']; ?></p>
                        <p><i class="fa fa-support"></i><?php echo $lang[$idioma]['LIST5_SAFE']; ?></p>
                    </div>
                    <!-- /col-lg-8 -->
                </div>
            </div>
            <!-- /container-->
        </div>
    </section>
    <!-- /Section ends -->

    <!-- Section Call to Action -->
    <section id="callout" class="">
        <!-- Clouds background -->
        <div class="hidden-xs">
            <div class="cloud x1"></div>
            <div class="cloud x2"></div>
            <div class="cloud x3"></div>
            <div class="cloud x4"></div>
            <div class="cloud x5"></div>
            <div class="cloud x6"></div>
            <div class="cloud x7"></div>
        </div>
        <!-- /Clouds ends -->
        <div class="container">
            <!-- Animated Sun -->
            <div class="sun hidden-sm hidden-xs">
                <div class="sun-face">
                    <div class="sun-hlight"></div>
                    <div class="sun-leye"></div>
                    <div class="sun-reye"></div>
                    <div class="sun-lred"></div>
                    <div class="sun-rred"></div>
                    <div class="sun-smile"></div>
                </div>
                <!-- Sun rays -->
                <div class="sun-anime">
                    <div class="sun-ball"></div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                    <div class="sun-light"><b></b><s></s>
                    </div>
                </div>
            </div>
            <!-- /Animated Sun -->
            <div class="col-md-4 col-sm-4 text-center">
                <div class="well">
                    <h3><?php echo $lang[$idioma]['VISITUS']; ?></h3>
                    <p></p>
                    <div class="page-scroll">
                        <!-- Button-->
                        <a class="btn" href="#contact">Contact us</a>
                    </div>
                    <!-- /page-scroll -->
                </div>
                <!-- /well -->
            </div>
            <!-- /col-md-6 -->
        </div>
        <!-- /container-->
    </section>
    <!-- Section ends -->

    <!--<section id="formparent" class="color-section">
        <svg class="triangleShadow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="trianglePath1" d="M0 0 L50 100 L100 0 Z"></path>
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="section-heading">
                        <h2><?php /*echo $lang[$idioma]['FORM']; */?></h2>
                    </div>
                </div>
                <div class="col-lg-6 col-lg-offset-3 text-center">
                    <h4>Write us</h4>
                    <div id="parent_form">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control input-field" placeholder="Name" required="">
                            <input type="email" name="email" class="form-control input-field" placeholder="Email"
                                   required="">
                            <input type="text" name="subject" class="form-control input-field" placeholder="Subject"
                                   required="">
                        </div>
                        <textarea name="message" id="message" class="textarea-field form-control" rows="4"
                                  placeholder="Enter your message" required=""></textarea>
                        <button type="submit" id="submit_btn" value="Submit" class="btn-mail btn center-block">Send message</button>
                    </div>
                    <div id="parent_results"></div>
                </div>

            </div>
    </section>-->
    <!--Section ends -->


    <!-- Parallax object -->
    <div class="parallax-object2 hidden-sm hidden-xs" data-0="opacity:1;"
         data-start="margin-top:50%"
         data-100="transform:translatey(0%);"
         data-center-center="transform:translatey(-180%);">
        <!-- Image -->
        <img src="img/parallaxobject2.png" alt="">
    </div>

    <!-- Section Gallery -->
    <section id="gallery" class="color-section">
        <!-- svg triangle shape -->
        <svg class="triangleShadow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100"
             viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="trianglePath1" d="M0 0 L50 100 L100 0 Z"/>
        </svg>
        <div class="container">
            <!-- Section heading -->
            <div class="section-heading">
                <h2>Our Gallery</h2>
            </div>
            <!-- Navigation -->
            <div class="text-center col-md-12">
                <ul class="nav nav-pills cat text-center" role="tablist" id="gallerytab">
                    <li class="active"><a href="#" data-toggle="tab" data-filter="*">All</a>
                    <li><a href="#" data-toggle="tab" data-filter=".events">Events</a></li>
                    <li><a href="#" data-toggle="tab" data-filter=".facilities">Our Facilities</a></li>
                </ul>
            </div>
            <!-- Gallery -->
            <div class="row">
                <div class="col-md-12">
                    <div id="lightbox">
                        <!-- Image 1 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 events">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery1.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery1.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 2 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery2.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery2.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 3 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery3.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery3.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 4 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 events">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery4.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery4.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 5 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery5.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery5.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 6 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery6.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery6.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 7 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 events">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery7.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery7.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 8 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 events">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery8.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery8.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 9 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery9.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery9.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 10 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery10.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery10.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 11 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery11.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery11.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 12 -->
                        <div class="col-sm-6 col-md-6 col-lg-4 facilities">
                            <div class="portfolio-item">
                                <div class="gallery-thumb">
                                    <img class="img-responsive" src="img/gallery12.jpg" alt="">
                                    <span class="overlay-mask"></span>
                                    <a href="img/gallery12.jpg" data-gal="prettyPhoto[gallery]" class="link centered"
                                       title="You can add caption to pictures.">
                                        <i class="fa fa-expand"></i></a>
                                </div>
                            </div>s
                        </div>
                    </div>
                    <!-- /lightbox-->
                </div>
                <!-- /col-md-12-->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </section>

   <!-- <section id="enroll" class="color-section">
        <svg class="triangleShadow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="trianglePath1" d="M0 0 L50 100 L100 0 Z"></path>
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">

                    <div class="section-heading">
                        <h2><?php /*echo $lang[$idioma]['ENROLL']; */?></h2>
                    </div>
                </div>

                <div class="col-lg-6 col-lg-offset-3 text-center">
                    <h4>Write us</h4>

                    <div id="parent_form">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control input-field" placeholder="Name" required="">
                            <input type="email" name="email" class="form-control input-field" placeholder="Email"
                                   required="">
                            <input type="text" name="subject" class="form-control input-field" placeholder="Subject"
                                   required="">
                        </div>
                        <textarea name="message" id="message" class="textarea-field form-control" rows="4"
                                  placeholder="Enter your message" required=""></textarea>
                        <button type="submit" id="submit_btn" value="Submit" class="btn-mail btn center-block">Send message</button>
                    </div>

                    <div id="parent_results"></div>
                </div>

            </div>
            
    </section>-->

    <!-- Section Contact -->
    <section id="contact" class="color-section">
        <svg class="triangleShadow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="trianglePath1" d="M0 0 L50 100 L100 0 Z"></path>
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <!-- Section heading -->
                <div class="section-heading">
                    <h2>Contact us</h2>
                </div>
            </div>
            <!-- Contact -->
            <div class="col-lg-4 text-center">
                <h4>Information</h4>
                <!-- contact info -->
                <div class="contact-info">
                    <p><i class="flaticon-back"></i><a href="mailto:info@littleharvardlearningcenter.com">info@littleharvardlearningcenter.com</a>
                    </p>
                    <p><i class="fa fa-phone margin-icon"></i>Call us 407-749-6664 </p>
                </div>
                <!-- address info -->
                <p>1060 cypress pkwy Kissimmee FL 34759</p>
                <!-- Map -->
                <div id="map-canvas">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3518.0230339693294!2d-81.4470746849283!3d28.145786982612314!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88dd9c147da70f5b%3A0x6130d482c8587e15!2s1060+Cypress+Pkwy%2C+Kissimmee%2C+FL+34759%2C+EE.+UU.!5e0!3m2!1ses-419!2sve!4v1563631706428!5m2!1ses-419!2sve"
                            width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <!-- Contact Form -->
            <div class="col-lg-7 col-lg-offset-1">
                <h4>Write us</h4>
                <!-- Form Starts -->
                <div id="contact_form">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control input-field" placeholder="Name" required="">
                        <input type="email" name="email" class="form-control input-field" placeholder="Email"
                               required="">
                        <input type="text" name="subject" class="form-control input-field" placeholder="Subject"
                               required="">
                    </div>
                    <textarea name="message" id="message" class="textarea-field form-control" rows="4"
                              placeholder="Enter your message" required=""></textarea>
                    <button type="submit" id="submit_btn" value="Submit" class="btn-mail btn center-block">Send message</button>
                </div>
                <!-- Contact results -->
                <div id="contact_results"></div>
            </div>
            <!--/Contact form -->
        </div>
        <!-- /container-->
    </section>
    <!--Section ends -->

    <!-- Footer -->
    <div class="container-fluid cloud-divider">
        <!-- Clouds SVG Divider -->
        <svg id="deco-clouds" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100"
             viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M-5 100 Q 0 20 5 100 Z
               M0 100 Q 5 0 10 100
               M5 100 Q 10 30 15 100
               M10 100 Q 15 10 20 100
               M15 100 Q 20 30 25 100
               M20 100 Q 25 -10 30 100
               M25 100 Q 30 10 35 100
               M30 100 Q 35 30 40 100
               M35 100 Q 40 10 45 100
               M40 100 Q 45 50 50 100
               M45 100 Q 50 20 55 100
               M50 100 Q 55 40 60 100
               M55 100 Q 60 60 65 100
               M60 100 Q 65 50 70 100
               M65 100 Q 70 20 75 100
               M70 100 Q 75 45 80 100
               M75 100 Q 80 30 85 100
               M80 100 Q 85 20 90 100
               M85 100 Q 90 50 95 100
               M90 100 Q 95 25 100 100
               M95 100 Q 100 15 105 100 Z">
            </path>
        </svg>
    </div>
    <footer class="color-section">
        <div class="container-fluid">
            <!-- Newsletter -->
            <!--<div class="col-lg-4 col-md-6 text-center res-margin">
               <h6 class="text-light">Sign our Newsletter</h6>
               <p>We will send updates once a week.</p>
               <div id="mc_embed_signup">
                  <form action="//yourlist.us12.list-manage.com/subscribe/post?u=04e646927a196552aaee78a7b&id=111" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                     <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                           <div class="input-group">
                              <input class="form-control input-lg required email" type="email" value="" name="EMAIL" placeholder="Your email here" id="mce-EMAIL">
                              <span class="input-group-btn">
                              <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn">
                              </span>
                           </div>

                           <div id="mce-responses" class="mailchimp">
                              <div class="alert alert-danger response" id="mce-error-response"></div>
                              <div class="alert alert-success response" id="mce-success-response"></div>
                           </div>
                        </div>

                     </div>
                  </form>
               </div>
            </div>-->
            <!-- /col-lg-4 -->
            <!-- Bottom Credits -->
            <div class="col-lg-6 col-md-6 res-margin">
                <a href="#page-top"><img src="img/logo.png" alt="" class="center-block"></a>
                <!-- social-icons -->
                <div class="social-media">
                    <a href="https://twitter.com/LittleHarvardL1" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.youtube.com/channel/UCS3daHM9cHa6z3iLxvxIBGA" target="_blank" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                    <a href="https://www.facebook.com/LittleHarvardLearningCenter/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/LittleharvardLC/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="whatsapp" data-telefono="+14072796089" title="Whatsapp"><i class="fa fa-whatsapp"></i></a>
                </div>
            </div>
            <!-- /col-lg-4 -->
            <!-- Opening Hours -->
            <div class="col-lg-6 col-md-12 text-center">
                <!-- Sign-->
                <h6 class="text-light"><span style="color:#2a6496;">Opening Hours:</span></h6>
                <!-- Table-->
                <table class="table">
                    <tbody>
                    <tr>
                        <td class="text-left"> <span style="color:#2a6496;">Monday to Friday</span></td>
                        <td class="text-right"><span style="color:#2a6496;">7 a.m. to 7 p.m.</span></td>
                    </tr>
                    <tr>
                        <td class="text-left"><span style="color:#2a6496;">Weekends / Holidays</span></td>
                        <td class="text-right"><span class="label label-danger">Closed</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /col-lg-4 -->
        </div>
        <!-- / container -->
        <p> &copy; Copyright, 2019 Little Hervard Learning Center - <a
                    href="http://xprmarketinggroup.com/" onMouseOver="this.style.cssText='color: #cc0000'"

                    onMouseOut="this.style.cssText='color: #009933'" class="xpr" target="_blank">
                <img src="img/logo_m.png" class="xpr" height="20"></a></p>
        <!-- /container -->
        <!-- Go To Top Link -->
        <div class="page-scroll hidden-sm hidden-xs">
            <a href="#page-top" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        </div>
    </footer>
    <!-- /footer ends -->
</div> <!-- /page width -->
<!-- Core JavaScript Files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Google maps -->
<script src="https://maps.googleapis.com/maps/api/js?v=3"></script>
<!-- Main Js -->
<script src="js/main.js"></script>
<!-- Isotope -->
<script src="js/jquery.isotope.js"></script>
<!--Mail Chimp validator -->
<script src='js/mc-validate.js'></script>
<!--Other Plugins -->
<script src="js/plugins.js"></script>
<!-- Contact -->
<script src="js/contact.js"></script>
<!-- Prefix free CSS -->
<script src="js/prefixfree.js"></script>
<!-- GreenSock -->
<script src="layerslider/js/greensock.js" type="text/javascript"></script>
<!-- LayerSlider script files -->
<script src="layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
</body>
</html>